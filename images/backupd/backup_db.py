#!/usr/bin/env python3

import os
import subprocess
from subprocess import PIPE, STDOUT, run
import datetime
import smtplib
from shutil import copy2
import tempfile
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

gendate = datetime.datetime.now()
strdate = gendate.strftime("%Y-%m-%d_%H-%M-%S")

print("Running Backup of database, please wait")

try:
    with tempfile.TemporaryDirectory() as bkp_dir_base:
        bkp_dir = os.path.join(bkp_dir_base, "backup_dbs")
        os.mkdir(bkp_dir)

        backup_file_base = 'database_%s_%s_%s.sql' % (os.environ.get('COMPOSE_PROJECT_NAME', 'exemplo'),
                                                      os.environ.get(
                                                          'MYSQL_DATABASE', 'exemplo.homolog'),
                                                      strdate)
        backup_file = '%s.zst' % (backup_file_base)

        ignore_tables = os.environ.get('MYSQLDUMP_IGNORED_TABLES', '').split(',') \
            if os.environ.get('MYSQLDUMP_IGNORED_TABLES', '') != '' else []

        ignore_tables_data = os.environ.get('MYSQLDUMP_IGNORED_TABLE_DATA', '').split(',') \
            if os.environ.get('MYSQLDUMP_IGNORED_TABLE_DATA', '') != '' else []

        extra_cmd = os.environ.get('MYSQLDUMP_EXTRA_ARGS', '')

        for item in ignore_tables:
            if '.' in item:
                extra_cmd += " --ignore-table=%s" % (item)
            else:
                extra_cmd += " --ignore-table=%s.%s" % (
                    os.environ.get('MYSQL_DATABASE', 'exemplo.homolog'), item)

        for item in ignore_tables_data:
            if '.' in item:
                extra_cmd += " --ignore-table-data=%s" % (item)
            else:
                extra_cmd += " --ignore-table-data=%s.%s" % (
                    os.environ.get('MYSQL_DATABASE', 'exemplo.homolog'), item)

        mariadb_dump = "%s " \
            "%s -h %s -P %s -u %s " \
            "--password=\"%s\" %s %s > %s" % (
                os.environ.get('MYSQL_PACKAGE_MAX', '--max_allowed_packet=1024M'),
                os.environ.get('MYSQLDUMP_PATH', '/usr/bin/mysqldump'),
                os.environ.get('MYSQL_HOST', 'mariadb'),
                os.environ.get('MYSQL_PORT', '3306'),
                os.environ.get('MYSQL_ROOT_USER', 'root'),
                os.environ.get('MYSQL_ROOT_PASSWORD', 'exemploSenha'),
                extra_cmd,
                os.environ.get('MYSQL_DATABASE', '--all-databases'),
                os.path.join(bkp_dir, backup_file_base)
            )
        subprocess.check_output(mariadb_dump, shell=True)

        bkpf = os.path.join(bkp_dir, backup_file)
        bkpfout = os.path.join(bkp_dir, backup_file + ".enc")

        keyFile = os.path.join(bkp_dir, backup_file + '.key.bin')
        encFile = os.path.join(bkp_dir, backup_file + '.key.bin.enc')

        zstd_compress = run('/usr/bin/zstd -19 -o "%s" < "%s"' %
                            (bkpf, os.path.join(bkp_dir, backup_file_base)), shell=True)

        run(['openssl', 'rand', '-base64', '-out', keyFile, '32'])
        run(['openssl', 'rsautl', '-encrypt', '-inkey',
            "/etc/public_key.pem", '-pubin', '-in', keyFile, '-out', encFile])
        run(['openssl', 'enc', '-aes-256-cbc', '-pbkdf2', '-iter', '100000', '-md',
            'sha512', '-salt', '-in', bkpf, '-out', bkpfout, '-pass', 'file:%s' % (keyFile)])

        # Decript

        # openssl rsautl -decrypt -inkey pv_key.pem -in InputKeyFile.key.bin.enc -out InputKeyFile.key.bin
        # openssl enc -d -aes-256-cbc -pbkdf2 -iter 100000 -md sha512 -salt -in InputBackupFile.sql.zst.enc -out OutputBackupFile.sql.zst -pass file:./InputKeyFile.key.bin
        # zstd -d OutputBackupFile.sql.zst
        # The output will be OutputBackupFile.sql

        # BACKUP TO RCLONE
        save_local = os.environ.get('BACKUPD_SAVE_LOCAL', '0')

        if save_local == "1":
            copy2(bkpfout, '/backupdir')
            copy2(encFile, '/backupdir')

        skip_rclone = os.environ.get('BACKUPD_SKIP_RCLONE', '0')

        if skip_rclone != '1':
            rclone = run(['/usr/bin/rclone', 'copy', '%s' %
                          bkpfout, 'backup:/home/sys_backup/backup_db/'])
            rclone.check_returncode()
            rclone = run(['/usr/bin/rclone', 'copy', '%s' %
                          encFile, 'backup:/home/sys_backup/backup_db/'])
            rclone.check_returncode()
except Exception as e:
    print("Backup error: %s" % str(e))
    from_email = os.environ.get('BACKUPD_SMTP_FROM_EMAIL')
    to_emails = os.environ.get('BACKUPD_SMTP_TO_EMAILS', '').split(',')

    smtp_address = os.environ.get('BACKUPD_SMTP_HOST')
    smtp_port = os.environ.get('BACKUPD_SMTP_PORT')
    smtp_user = os.environ.get('BACKUPD_SMTP_USER')
    smtp_password = os.environ.get('BACKUPD_SMTP_PASSWORD')

    for to_email in to_emails:
        print("Enviando email para: %s" % to_email)
        msg = MIMEMultipart()
        msg['From'] = from_email
        msg['To'] = to_email
        msg['Subject'] = "[BACKUP] %s do Projeto %s teve um erro: %s" % (os.environ.get('MYSQL_DATABASE', 'iapag.reactiva.homolog'),
                                                                         os.environ.get(
                                                                             'COMPOSE_PROJECT_NAME', 'iapag.homolog'),
                                                                         strdate)
        body = "Ocorreu um erro durante o backup do banco de dados do projeto, favor verificar: Erro - {0}".format(
            str(e))
        msg.attach(MIMEText(body, 'plain'))

        s = smtplib.SMTP(smtp_address, smtp_port)
        s.starttls()
        s.login(smtp_user, smtp_password)

        text = msg.as_string()
        s.sendmail(from_email, to_email, text)
        s.quit()

    raise e

print("Backup complete")
