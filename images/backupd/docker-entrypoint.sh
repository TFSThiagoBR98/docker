#!/bin/bash
set -eo pipefail
shopt -s nullglob

/usr/bin/env > /etc/environment

printenv | sed 's/^\(.*\)$/export \"\1\"/g' > /root/project_env.sh

exec /usr/sbin/cron -f