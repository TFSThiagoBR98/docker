#!/bin/bash

if [ "$SKIP_PRE_START" != "true" ]; then
    if [ -f "composer.json" ]; then
        echo "Starting PHP Composer deployment...";
        composer install;
    fi

    if [ -f "package.json" ]; then
        echo "Starting NPM Node deployment...";
        npm install;
    fi

    if [ -f "artisan" ]; then
        echo "Laravel detected, deploying...";
        php artisan storage:link;
    fi
fi

/usr/bin/supervisord -n -c /etc/supervisord.conf