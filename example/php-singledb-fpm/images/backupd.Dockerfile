FROM registry.gitlab.com/tfsthiagobr98/docker/backupd:latest

COPY ./data/rclone.conf /config/rclone/rclone.conf
COPY ./data/backupd.json /etc/backup_info.json